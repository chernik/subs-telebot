import telebot, json, os, sys, traceback, time

class Data:
	def loadData():
		if not os.path.exists('data.json'):
			raise Exception('Create data.json in that dir and put there {"token": <your token>}');
		with open('data.json', 'r') as f:
			d = json.load(f)
		if d.get('users') == None:
			d['users'] = {}
			Data.saveData(d)
		return d

	def saveData(d):
		with open('data.json', 'w') as f:
			json.dump(d, f)

	def getBot():
		if Data.__dict__.get('bot') != None:
			return Data.bot
		d = Data.loadData()
		Data.bot = telebot.TeleBot(d['token'])
		return Data.bot

	def getUser(uid):
		d = Data.loadData()
		if uid in d['users']:
			return d['users'][uid]
		else:
			return None

	def newUser(uid):
		d = Data.loadData()
		d['users'][uid] = {
			'uid': uid
		}
		Data.saveData(d)
		return d['users'][uid]

def startServer():
	bot = Data.getBot()
	@bot.message_handler(content_types=['text'])
	def onMessage(mes):
		handleMes(bot, mes)
	while True:
		try:
			print("Polling...")
			bot.polling(none_stop = True, interval = 0)
			print("Polled!")
			break
		except KeyboardInterrupt:
			break
		except:
			traceback.print_exc()

def handleMes(bot, mes):
	uid = mes.from_user.id
	print('Recieved from %s mes %s' % (str(uid), str(mes.text)))
	if mes.text == '/start':
		u = Data.getUser(uid)
		if u == None:
			us = Data.newUser(uid)
			bot.send_message(uid, 'You registred!')
		else:
			bot.send_message(uid, 'You already registred!')
		return

def sendAll(mes):
	d = Data.loadData()
	bot = Data.getBot()
	for uid in d['users']:
		bot.send_message(uid, mes)

def wrapDocker(lbl, argv):
	os.system('docker rm -f "%s"' % lbl)
	os.system('docker build -t "%s" .' % lbl)
	os.system('docker run -d --name "%s" "%s" sh /root/main.sh %s' % (
		lbl, lbl,
		' '.join(['"%s"' % arg for arg in argv])
	))
	print('Container name is [%s]' % lbl)

def pingingHost(host, timeout, mes):
	while True:
		res = os.system('ping -c 4 "%s"' % host)
		if res != 0:
			sendAll(mes)
		time.sleep(timeout)

def main():
	if len(sys.argv) < 2:
		print("""
Usage:
	<app>
	[ -d <docker container label> ]
	[
		-s |
		-m <message> |
		-p <timeout> "<message on fail>" <host>
	]
		""")
		return
	if sys.argv[1] == '-s':
		startServer()
		return
	if sys.argv[1] == '-m':
		sendAll(sys.argv[2])
		return
	if sys.argv[1] == '-d':
		if len(sys.argv) < 3:
			print('Wrong -d option')
			return
		wrapDocker(sys.argv[2], sys.argv[3:])
		return
	if sys.argv[1] == '-p':
		if len(sys.argv) < 5:
			print('Wrong -p option')
			return
		pingingHost(sys.argv[4], int(sys.argv[2]), sys.argv[3])
		return
	print('Wrong usage')

main()
