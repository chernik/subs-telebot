import os, sys

def initPenv():
	if os.path.exists('penv'):
		return
	os.system('python3 -m venv penv')
	os.system('. penv/bin/activate && python3 init.py -pip && deactivate')

def initPip():
	os.system('pip install --upgrade pip')
	os.system('pip install -r req.txt')

def main():
	if len(sys.argv) == 1:
		initPenv()
	elif sys.argv[1] == '-pip':
		initPip()

main()
