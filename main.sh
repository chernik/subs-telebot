#/bin/sh

cd "${0%/*}"
python3 init.py

. penv/bin/activate
python3 main.py "$@"
deactivate
