# What is that?

Thats pretty script for telegram bot

# Whats about requirenments?

- linux
- python3
- pip3
- venv

# How to use?

### Create telegram bot

Find *@BotFather* bot in Telegram, follow instructions and you get token for your bot

### Create data file

Create data.json and write that  
`{"token": "<your token>"}`

### Start server

Shell:  
`./main.sh -s`

### Send potential subscribers nickname of your bot

And thay should write */start* to that bot

### Send message to all subscribers

Shell:  
`./main.sh -m "<your message>"`

### Periodic ping some host

And send all subscribers about fail when ping failed

Shell:  
`./main.sh -p <timeout> "<message on fail>" <host>`

- *<timeout>* is time in seconds, thats the period of checks

### Pack to docker

Firstly install docker

We can use any option of that script. For example, if you run that:  
`./main.sh -s`

Server will be run in your console. But if you run that:  
`./main.sh -d some_bot_server -s`

Script created the docker container with name *some_bot_server* and run detached server into them

Usage:  
`./main.sh -d <container name> <any arg1> <any arg2> ...`
